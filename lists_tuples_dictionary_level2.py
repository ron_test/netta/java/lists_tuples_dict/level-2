#lists_tuples_dictionary - level 2 - 1
def two_sum():
    """
    Given an array of integers nums and an integer target.
    return: indices of the two numbers such that they add up to target.
    """
    nums_input = input("Please enter the list of numbers separated by commas: ")
    nums = [int(num) for num in nums_input.split(",")]
    nums_len = len(nums)
    target = int(input("Please insert the target number: "))
    for i in range(0, nums_len):
        for k in range(0, nums_len):
            temp = nums[i] + nums[k]
            if temp == target:
                return print(f"[{i}, {k}]")


two_sum()






#lists_tuples_dictionary - level 2 - 2
def median_of_Two_Sorted_Arrays():
    """
    function that gets from the user two sorted arrays.
    return: the median of the two sorted arrays.
    """
    arr1_input = input("Please enter the list of numbers separated by commas: ")
    arr1 = [int(num) for num in arr1_input.split(",")]
    arr1_len = len(arr1)
    arr2_input = input("Please enter the list of numbers separated by commas: ")
    arr2 = [int(num) for num in arr2_input.split(",")]
    arr2_len = len(arr2)
    arr1.extend(arr2)
    merged_arr_len = arr1_len + arr2_len
    arr1.sort()
    median = 0.0
    if merged_arr_len % 2 == 0:
        median = (arr1[(merged_arr_len // 2) - 1] + arr1[merged_arr_len // 2]) / 2
    else:
        median = arr1[merged_arr_len // 2]
    return print(median)


median_of_Two_Sorted_Arrays()






#lists_tuples_dictionary - level 2 - 3
def merge_sorted_lists(arr):
    """
    function that gets an array with lists, and k = her length.
    return: merged sorted list.
    """
    merged_list = []
    for sublist in arr:
        merged_list.extend(sublist)
    merged_list.sort()
    return merged_list


arr = []
k = int(input("please enter how many lists you want: "))
for i in range(0, k):
    arr_input = input("Please enter the list of numbers separated by commas: ")
    arr.append([int(num) for num in arr_input.split(",")])
print(merge_sorted_lists(arr))






#lists_tuples_dictionary - level 2 - 4
class ListNode:
    """
    Class that defines a linked list node.
    """
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

def reverse_k_group(head, k):
    """
    Function that reverses nodes in k groups in a linked list.
    """
    def reverse_sublist(first_node, last_node):
        """
        Helper function to reverse a sublist.
        """
        prev = None
        current = first_node
        while current != last_node:
            next_node = current.next
            current.next = prev
            prev, current = current, next_node
        return prev
    temp = ListNode(0)
    temp.next = head
    prev_group_last_node = temp
    current_group_first_node = head
    while True:
        group_len = k
        group_last_node = current_group_first_node
        while group_len > 0 and group_last_node:
            group_last_node = group_last_node.next
            group_len -= 1
        if group_len > 0:
            break
        next_group_first_node = group_last_node.next
        prev_group_last_node.next = reverse_sublist(current_group_first_node, group_last_node)
        current_group_first_node.next = next_group_first_node
        prev_group_last_node, current_group_first_node = current_group_first_node, group_last_node
    return temp.next



node1 = ListNode(1, ListNode(2, ListNode(3, ListNode(4, ListNode(5)))))
k = 2
result = reverse_k_group(node1, k)
while result:
    print(result.val, end=" -> ")
    result = result.next

